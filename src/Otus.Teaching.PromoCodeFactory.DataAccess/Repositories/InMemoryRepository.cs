﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        readonly ConcurrentDictionary<Guid, T> _data;

        public InMemoryRepository(IEnumerable<T> data) {
            _data = new ConcurrentDictionary<Guid, T>(data.Select(x => new KeyValuePair<Guid, T>(x.Id, x)));
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(_data.Values.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.Run(() => {
                if (!_data.TryGetValue(id, out T value)) {
                    Debug.WriteLine($"Error in getting {nameof(T)} with id = {id}");
                }
                return value;
            });
        }

        public Task UpdateAsync(T entity) {
            return Task.Run(() => {
                if (!TryUpdate(entity)) {
                    Debug.WriteLine($"Error in updating {nameof(T)} with id = {entity.Id}");
                }
            });
        }

        public Task AddAsync(T entity) {
            return Task.Run(() => {
                if (!_data.TryAdd(entity.Id, entity)) {
                    Debug.WriteLine($"Error in adding {nameof(T)}");
                }
            });
        }

        public Task DeleteAsync(Guid id) {
            return Task.Run(() => {
                if (!_data.TryRemove(id, out _)) {
                    Debug.WriteLine($"Error in removing {nameof(T)} with id = {id}");
                }
            });
        }

        private bool TryUpdate(T entity) {
            while (_data.TryGetValue(entity.Id, out T existingValue)) {
                if (_data.TryUpdate(entity.Id, entity, existingValue))
                    return true;
                // if we're looping either the key was removed by another thread,
                // or another thread changed the value, so we start again.
            }
            return false;
        }
    }
}