﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers {
    public class AutoMapperProfile : Profile {

        public AutoMapperProfile() {

            CreateMap<Employee, EmployeeResponse>()
                .ForMember(x => x.FullName, opt => opt.MapFrom(x => $"{x.FirstName} {x.LastName}"));

            CreateMap<Employee, EmployeeShortResponse>()
                .ForMember(x => x.FullName, opt => opt.MapFrom(x => $"{x.FirstName} {x.LastName}"));

            CreateMap<EmployeeCreatingDto, Employee>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => Guid.NewGuid()));

            CreateMap<EmployeeUpdatingDto, Employee>();

            CreateMap<Role, RoleItemResponse>().ReverseMap();
        }
    }
}
